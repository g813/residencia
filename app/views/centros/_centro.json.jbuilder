json.extract! centro, :id, :sigla, :nome, :created_at, :updated_at
json.url centro_url(centro, format: :json)
