class CreateCentros < ActiveRecord::Migration[7.1]
  def change
    create_table :centros do |t|
      t.string :sigla
      t.string :nome

      t.timestamps
    end
  end
end
